import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(primarySwatch: Colors.deepPurple),
      home: Scaffold(
        backgroundColor: Colors.blue.shade700,
        appBar: AppBar(
          backgroundColor: Colors.lightBlue,
          title: Text("Welcome"),
        ),
        body: SafeArea(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              CircleAvatar(
                backgroundColor: Colors.blueAccent,
                radius: 50,
                backgroundImage:
                    AssetImage("images/LOGO.png"),
              ),
              Text(
                "ansadav",
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Colors.white,
                    fontSize: 40,
                    fontFamily: "Srisakdi"),
              ),
              Text("Best E-Commerce platform",
              style:TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 20,
                fontFamily: "SourceSansPro",
                color:Colors.white,
              ),
              ),
              SizedBox(
                height: 20,
                width:150,
                child:Divider(
                  color:Colors.teal.shade100,
                ),
              ),
              Card(

                color: Colors.white,
                margin: EdgeInsets.only(left:10,right:10,top:10),
                child:ListTile(
                  leading: Icon(Icons.call,color: Colors.blue.shade700,),
                  title: Text("+233246782984",style:TextStyle(
                    fontFamily: "SourceSansPro",
                    fontSize: 20,
                    color:Colors.blue.shade700,
                  ),),
                ),

              ),
              SizedBox(
                height:15,
              ),

              Card(
                color: Colors.white,
                margin: EdgeInsets.only(left:10,top:10,right:10),
               child:ListTile(
                 leading: Icon(Icons.email,color:Colors.blue.shade700),
               title:  Text("akandimichael@yahoo.com",style:TextStyle(
                   fontFamily: "SourceSansPro",
                   fontSize: 20,
                   color:Colors.blue.shade700,

                 ),),
               ),




              ),
            ],
          ),
        ),
      ),
    );
  }
}

